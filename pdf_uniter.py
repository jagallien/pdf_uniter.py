
import PyPDF2
import sys

#TODO create 'try' & 'catch'/'exception' for arg-passing.
#Should be able to tell you whether or not file exists, etc.

if len(sys.argv) < 4:
    print("\nUsage: " + sys.argv[0] + " firstfile.pdf secondfile.pdf output_file.pdf\n")
else:
    pdf1 = open(sys.argv[1], 'rb')
    pdf2 = open(sys.argv[2], 'rb')
    fileout = open(sys.argv[3], 'wb')
    
    merger = PyPDF2.PdfFileMerger()

    merger.append(pdf1)
    merger.append(pdf2)
    merger.write(fileout)

