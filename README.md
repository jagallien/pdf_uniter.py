# pdf_uniter.py
PDF1 + PDF2 = PDF3 (Because [Adobe wants $5/mo] for that feature)

## Requirements:
* [Python3]
* [PyPDF2]
* 2 PDFs
* You don't need this if you run Linux, there's [already a program that does this.]

## Usage
* pdf_uniter.py firstfile.pdf secondfile.pdf output_file.pdf
* example: pdf_uniter.py mydocument_page1.pdf mydocument_page2.pdf mynewdocument.pdf

[Python3]: https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe
[already a program that does this.]: https://www.unix.com/man-page/centos/1/pdfunite/
[PyPDF2]: https://pythonhosted.org/PyPDF2/
[Adobe wants $5/mo]: https://acrobat.adobe.com/us/en/landing/pdf-pack-combine.html?trackingid=QBWYPQS6&ttid=cmbnfls1&DTProd=Reader&DTServLvl=SignedOut&ttsrccat=IPM*RDR12-ALL-ACOM-201711*All*IPM*RHPToolAdd*RPO*Var7
